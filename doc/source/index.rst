.. IOTA2 documentation master file, created by
   sphinx-quickstart on Wed Jun  6 12:37:42 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to IOTA²'s documentation !
==================================

.. Note::
    This short documentation was written to quickly help users to run IOTA², and
    developers to contribute to the project. It is not complete and most parts still in development.

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :doc:`Get IOTA² <HowToGetIOTA2>`
* :doc:`IOTA² Examples <IOTA2_Example>`
* :doc:`Development recommendations <develop_reco>`
* :doc:`IOTA² code architecture : main class <main_class>`
* :doc:`Add steps to IOTA² <add_IOTA2_new_step>`
* :doc:`Sentinel-2 Level 3A <sentinel_2_N3A>`

